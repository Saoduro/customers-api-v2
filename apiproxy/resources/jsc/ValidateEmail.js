 var email = context.getVariable("request.header.email");
 var timeStamp = new Date().toISOString().replace('Z', '');
 context.setVariable("timeStamp", timeStamp);
 
context.setVariable("isValidEmail","true");
var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);

if (email !== null) {
    if(email === null || !regex.test(email)) {
    context.setVariable("isValidEmail","false");
}
}